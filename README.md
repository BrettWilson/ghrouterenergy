# README #

This library uses the Graphhopper 0.8 Java low level API to implement flexible Electric Vechicle routing (as well as the standard fastest and shortest routes)


### Quick setup ###

1. (Open the terminal) Clone the repo by generating the clone link in the top left corner under "ACTIONS"
2. Navigate into the root folder of the project and complete a full build by running the "fullbuild" script command (GHRouter.sh)
3. Test a random route by using the "random" script command  
Note: the first time you test a route it will need to download the map data (.osm/.pbf file) and elevation data and generate the nodes/edges - this takes quite a while. The downloading is all done automatically just press the enter key when prompted

```
#!bash
git clone <link>
cd ghrouterenergy
./GHRouter.sh fullbuild
./GHRouter random 1
```
### Super Quick setup ###
Setup project and generate a random route in one line
```
#!bash

git clone link && cd ghrouterenergy && ./GHRouter.sh fullbuild && ./GHRouter.sh random 1

```

### Commands and how to use the library ###

Once you have setup the project on your local machine you can easily generate random routes, find routes given lat/lon coords from a file or from standard.in (System.in in Java or from piping).  
**Note: you only need to build the project when you have edited the GHRouter source code and you only need to fullbuild the project once to generate the core .jar files and then never again unless you edit the graphhopper core files (e.g. you edit the EVWeighting class etc)**

A valid line of input is : <latFrom lonFrom latTo lonTo> in decimal format (E.G. -37.788624 175.316555 -37.744318 175.240173)  
You may give the program as many valid lines as you want  

Possible commands  

```
#!bash

./GHRouter.sh help            - print this message  
./GHRouter.sh build           - build project - make sure to do this at least once and then whenver you change the source code  
./GHRouter.sh fullbuild       - build project AND build graphhopper main project - not neccessary unless you changed the core GH code  
./GHRouter.sh random n        - generate n lines of random input from .osm/.pbf file specified in random-config.properties  
./GHRouter.sh file <filename> - read valid input from file  
```


Example commands:  
See script help message  

```
#!bash

./GHRouter help
```  

Generate 10 random routes

```
#!bash

./GHRouter random 10
```  

Read input from input.txt (one per line from a file)
```
#!bash

./GHRouter file input.txt
```  

Read input using piping
```
#!bash

echo "-37.788624 175.316555 -37.744318 175.240173" | ./GHRouter.sh

``` 
or 
```
#!bash

cat input.txt | ./GHRouter.sh

``` 

### Configuration ###
There are actually two projects:  
1. GHRouter - the project present in the root folder, contains GHRouter.java and GHRouterDebug.java. GHRouter contains the code which is used by the script and is fully commented.  
2. graphhopper - the Graphhopper 0.8 low level Java API, this contains the core library source code, which has been edited to add Electric Vechicles support   

The build command builds the GHRouter library, where as the fullbuild command builds both libraries.

### GHRouter config ###
The config.properties file in the root folder contains the default configuration and is already setup to work out-of-the-box.  
The main properties you may want to change is the map file and elevation source (although the current elevation source; cgiar is meant to be the most accuarate)  
graph.location = where graph files are stored  
datareader.file= the name of the map file, this is currently australia-oceania_new-zealand.pbf this downloads the whole of NZ  

**IMPORTANT** : there is another config file, random-config.properties that is used when using the "random" command in GHRouter. This will load a smaller .osm/.pbf file for generating more realistic routes. If you use a full country or larger it will generate mostly very long routes. Some even over water (which it can deal with via ferries - but we are not interested in this.) The smaller file is hamilton, NZ by default.

### Edited/added Graphhopper source code ###
The Electric vechicle consumption model is implemented as a new Weight class in the GH low level api called [EVWeighting.java](https://bitbucket.org/BrettWilson/ghrouterenergy/src/f40de103ed3a7b696afb3819028e146dca3e4136/graphhopper/core/src/main/java/com/graphhopper/routing/weighting/EVWeighting.java?at=master&fileviewer=file-view-default). The Graphhopper main class [GraphHopper.java](https://bitbucket.org/BrettWilson/ghrouterenergy/src/f40de103ed3a7b696afb3819028e146dca3e4136/graphhopper/core/src/main/java/com/graphhopper/GraphHopper.java?at=master&fileviewer=file-view-default) has also been edited slightly to add EV weight support. Other classes such as Path, PathWrapper have been edited to always keep track of energy consumption - even when calculating the fastes/shortest routes (so you can compare). Note that this only works for standard Dijkstra algorthim

### Using the web interface useful map UI ###
The graphopper low level api has a nice web interface that looks similar to google maps. To run it, navigate into the graphopper folder and use the script file.

```
#!bash
cd graphhopper/
./graphhopper.sh web australia-oceania_new-zealand.pbf 

```  
Head to http://localhost:8989/ in your web browser to generate routes. You can right click on the map to set start and end point  
**To use the energy or shortest weight, change weighting=fastest in the url to energy or shortest**  
You can replace "australia-oceania_new-zealand.pbf" just like the other script.

### Eletric vechicle model ###
The electric vechicle model I used was from:  
```
T.  Jurik, A.  Cela, R.  Hamouche, R.  Natowicz, A.  Reama, S.  Niculescu and J.  Julien, "Energy Optimal Real-Time Navigation System", IEEE Intelligent Transportation Systems Magazine, vol. 6, no. 3, pp. 66-79, 2014.
```  
I have included this in the root folder of the project or click [here](https://bitbucket.org/BrettWilson/ghrouterenergy/src/69d64dafde8d7e65541692d3c42840c178dd0ec5/Energy_Optimal_Real-Time_Navigation_Syst.pdf?at=master&fileviewer=file-view-default)  
The model I use is equation (7) in that paper and the values for the constants are given in table 1 for that paper (except for g, which I assumed was the [gravitational acceleration constant](https://en.wikipedia.org/wiki/Gravitational_acceleration))  
Note that all the constants are declared in the [EVWeighting.java](https://bitbucket.org/BrettWilson/ghrouterenergy/src/f40de103ed3a7b696afb3819028e146dca3e4136/graphhopper/core/src/main/java/com/graphhopper/routing/weighting/EVWeighting.java?at=master&fileviewer=file-view-default) Java class in the Graphhopper core. That is the best class to read for a quick overview of the model  

Problems I have found with this model (could be model or I could be implementing it wrong)  
1.  The speed squared part really makes the number so much bigger, roads with high speeds e.g. motorways/highways where it will be 100 seem to be punished too much 100x100 >>> 50x50  
2.  There is a big difference between cos(theta) and sin(theta) sin is usually much smaller number e.g. 0.01 vs 0.1 it is not clear why cos is used instead of sin and vice versa  
3.  Ep is very small compared to Er, so small it may as well not be there in some cases  

### Extra help ###
For the Graphhopper docs: (and other readme files in the same directory)  
https://github.com/graphhopper/graphhopper/blob/master/docs/core/quickstart-from-source.md