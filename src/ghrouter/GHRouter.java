package ghrouter;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import com.graphhopper.reader.osm.GraphHopperOSM;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.util.CmdArgs;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

/**
 * Simple program which generates the fastest, shortest and energy routes for given to/from pairs or 
 * randomly generated ones.  
 * A valid line of input is : latFrom lonFrom latTo lonTo in decimal format
 * You may give the program as many valid lines as you want either through a file or piping (System.in)
 * E.G. -37.788624 175.316555 -37.744318 175.240173 
 * @author bkw5
 */
public class GHRouter {   
    
    public static void main(String[] args) throws IOException {        

         // Input will contain a list of lines of valid input, one member == one line
        LinkedList<String> input = parseArgs(args);
        
        // No input
        if(input == null || input.isEmpty()){
            System.out.println("No input was successfully parsed - empty file/input?");
            return;
        }

        // Init hopper with settings from config file
        GraphHopper hopper = new GraphHopperOSM().forServer()
                .init(CmdArgs.readFromConfig("config.properties", "graphhopper.config"));  

        // Load/download graph -- This takes a while the first time it is done
        // Internet connection required to download the map data
        hopper.importOrLoad();      
        
        // Find fastest, shortest and best energy route for each line of input
        int iter = 0;
        for (String line : input) {
            iter++;            
            double[] coords = validateInput(line.split(" "), iter);
            route("car", hopper, coords[0], coords[1], coords[2], coords[3]);            
        }

    }

    /**
     * Find fastest, shortest and least energy route for the given start and end points
     * @param vehicle - use car, you may use other vehicles but the energy consumption algorithm is for a car
     * @param hopper - initialized GH instance - i.e. GH is loaded with config and graph (.osm/.pbf) 
     * @param latFrom - lat of start node
     * @param lonFrom - lon of start node
     * @param latTo   - lat on destination node
     * @param lonTo   - lon on destination node
     */
    private static void route(String vehicle, GraphHopper hopper,
            double latFrom, double lonFrom, double latTo, double lonTo) {
        
        // Normal Dijkstra is used here
        // Note: in order to keep track of the energy used during fastest/shortest
        // routes I had to edit the source code, for some other algorithms the energy may not be tracked correctly
        // just stick with dijkstra for now - it is more than fast enough
        GHRequest req = new GHRequest(latFrom, lonFrom, latTo, lonTo).
                setWeighting("energy").
                setVehicle(vehicle).
                setLocale(Locale.getDefault()).
                setAlgorithm("dijkstra");        
        
        GHRequest req2 = new GHRequest(latFrom, lonFrom, latTo, lonTo).
                setWeighting("fastest").
                setVehicle(vehicle).
                setLocale(Locale.getDefault()).
                setAlgorithm("dijkstra");
        
        GHRequest req3 = new GHRequest(latFrom, lonFrom, latTo, lonTo).
                setWeighting("shortest").
                setVehicle(vehicle).
                setLocale(Locale.getDefault()).
                setAlgorithm("dijkstra");        
       
        
        // Find best route
        GHResponse rsp2 = hopper.route(req2);
        GHResponse rsp3 = hopper.route(req3);
        GHResponse rsp = hopper.route(req);        
        
        
        // First check for errors
        if (rsp.hasErrors() || rsp2.hasErrors() || rsp3.hasErrors()) {
            System.out.println("ERROR in calculating route");
            System.out.println(rsp.getErrors());
            System.out.println(rsp2.getErrors());
            System.out.println(rsp3.getErrors());
            return;
        }
        
        // Grab the best route
        // e energy, f fastest, s shortest
        PathWrapper epath = rsp.getBest();
        PathWrapper fpath = rsp2.getBest();
        PathWrapper spath = rsp3.getBest();
        
        // Get the weight of the energy route i.e lowest energy used
        double bestEnergy = epath.getRouteWeight();
        // Get the weight of the fastest route i.e fatest time
        long fastestTime = fpath.getTime();
        // Get the weight of the shortest route i.e lowest distance travelled
        double lowestDistance = spath.getRouteWeight();
        
        // Since paper does not state otherwise, I am assumining energy is measured in Joules
        // To convert to Wh (watt hour) divide it by 3600 (or 3600*1000 for KWh)
        double scaleWh = 3600.0;
        double scaleKWh = 3600000.0;
        
        // Print stats, feel free to change format or print more info
        // E.g. path.getInstructions will get turn instructions (turn right etc) see GHRouterDebug for example
        System.out.println("Routing: " + latFrom + ", "+ lonFrom+ " ---> "+ latTo+ ", "+lonTo);
        System.out.println("Fastest Route:      " + toKm(fpath.getDistance()) + "km, " + convertSecondsToMmSs(fpath.getTime())
        + ", " + String.format("%.3f", fpath.getRouteCost()/scaleKWh)+"KWh");
        System.out.println("Shortest Route:     " + toKm(spath.getDistance()) + "km, " + convertSecondsToMmSs(spath.getTime())
        + ", " + String.format("%.3f", spath.getRouteCost()/scaleKWh)+"KWh");
        System.out.println("Energy Route:       " + toKm(epath.getDistance()) + "km, " + convertSecondsToMmSs(epath.getTime())
        + ", " + String.format("%.3f", epath.getRouteCost()/scaleKWh)+"KWh");
        System.out.println("Energy vs Fastest:  " + toKm(epath.getDistance() - fpath.getDistance()) + "km, " + 
                 convertSecondsToMmSs(epath.getTime() - fastestTime) + ", " + 
                String.format("%.2f",-((fpath.getRouteCost() - bestEnergy) / fpath.getRouteCost())*100) + "% (" +
                 String.format("%.3f", (bestEnergy-fpath.getRouteCost())/scaleKWh) + "KWh)");
        System.out.println("Energy vs Shortest: " + toKm(epath.getDistance() - spath.getDistance()) + "km, " + 
                 convertSecondsToMmSs(epath.getTime() - spath.getTime()) + ", " + 
                String.format("%.2f",-((spath.getRouteCost() - bestEnergy) / spath.getRouteCost())*100) +"% (" +
                 String.format("%.3f", (bestEnergy-spath.getRouteCost())/scaleKWh) + "KWh)\n");
        
    }
  
    /**
     * Deals with arguments
     * No arguments == expects input from system.in
     * Otherwise expects <random n> or <file filepath>
     * Otherwise bad args
     * @param args
     * @return List of strings, where one member is one line of input
     * @throws IOException 
     */
    private static LinkedList<String> parseArgs(String[] args) throws IOException {
        LinkedList<String> input = new LinkedList<>();
        Scanner scanner;

        if (args.length == 0) {
            scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                input.add(scanner.nextLine());
            }
        } else if (args.length == 2 && !args[1].isEmpty()) {
            if (args[0].equalsIgnoreCase("random")) {
                return generateRandomInput(args[1]);
            } else if (args[0].equalsIgnoreCase("file")) {
                return readInputFromFile(args[1]);
            } else {
                System.out.println("Argument " + Arrays.toString(args) + " is not valid");
                printHelp();
                System.exit(1);
            }
        } else {
            System.out.println("Argument " + Arrays.toString(args) + " is not valid");
            printHelp();
            System.exit(1);
        }

        return input;
    }

    /**
     * Generates n lines of random input - so n lines of <latFrom lonFrom latTo lonTo> in decimal format
     * Each line is generated by selecting two random nodes in the graph (.pbf/.osm file)
     * @param num - amount of random lines of input to generate
     * @return List of String, each member represents one line of input
     * @throws IOException 
     */
    private static LinkedList<String> generateRandomInput(String num) throws IOException {
        LinkedList<String> input = new LinkedList<>();
        int n = 0;
        try{
            n = Integer.parseInt(num);
        } catch (NumberFormatException e){
            System.out.println("Argument " + num + " is not an int, expecting random <int>");
            printHelp();
            System.exit(1);
        }
        
        // Random number generater with default seed is fine here
        Random rand = new Random();
        
         // Init hopper with settings from config file
         // This will load a smaller .osm/.pbf file for generating more realistic routes
         // If you use a full country or larger it will generate mostly very long routes
         // Some even over water (which it can deal with via ferries - but we are not interested in this.
        GraphHopper hopper = new GraphHopperOSM().forServer()
                .init(CmdArgs.readFromConfig("random-config.properties", "graphhopper.config"));  

        // Load/download graph -- This takes a while the first time it is done
        hopper.importOrLoad();
        
        // Get underlying graph and node access
        GraphHopperStorage g = hopper.getGraphHopperStorage();
        NodeAccess na        = g.getNodeAccess();
        // Get max index of a node
        int maxNode          = g.getNodes(); 
        
        for(int i = 0; i < n; i++){
            // Get two random nodes from graph
            int from = rand.nextInt(maxNode);
            int to = rand.nextInt(maxNode);            
            input.add(""+na.getLatitude(from)+" "+na.getLongitude(from)
                    +" "+na.getLatitude(to)+" "+na.getLongitude(to));
        }

        return input;
    }

    /**
     * Reads valid input from a file
     * @param file
     * @return 
     */
    private static LinkedList<String> readInputFromFile(String file) {
        LinkedList<String> input = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(file));
            while (scanner.hasNextLine()) {
                input.add(scanner.nextLine());
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(GHRouter.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Could not find or open file " + file);
            System.out.println(ex.getLocalizedMessage());
            System.exit(1);
        }
        return input;
    }

    /**
     * Print help message
     */
    private static void printHelp() {
        System.out.println("Valid options are:");
        System.out.println("file <filename>      - load valid input from a file");
        System.out.println("random <n>           - generate n random lines of input");
        System.out.println("<no args>            - will try and read input from system.in");
        System.out.println("Valid input is <latFrom lonFrom latTo lonTo> in decimal format, one per line");
    }

    /**
     * Checks if a line of input (after going line.split(" ")) is valid
     * @param split - line.split(" ")
     * @param lineNumber - current line we are working on
     * @return Return coords [latFrom, lonFrom, latTo, lonTo]
     */
    private static double[] validateInput(String[] split, int lineNumber) {
        if (split.length != 4) {
            System.out.println("Invalid input on line " + lineNumber + " only " + split.length
                + " values (seperated by space), expecting 4");
            System.out.println("Input was: " + Arrays.toString(split));
            printHelp();
            System.exit(1);
        }
        double latFrom = 0, lonFrom = 0, latTo = 0, lonTo = 0;
        try {
            latFrom = Double.parseDouble(split[0]);
            lonFrom = Double.parseDouble(split[1]);
            latTo = Double.parseDouble(split[2]);
            lonTo = Double.parseDouble(split[3]);
        } catch (NumberFormatException numberFormatException) {
            System.out.println("Invalid input on line " + lineNumber);
            System.out.println("Failed to parse double (coord) " + numberFormatException.getLocalizedMessage());
            System.out.println("Input was: " + Arrays.toString(split));
            printHelp();
            System.exit(1);
        }
        return new double[]{latFrom, lonFrom, latTo, lonTo};
    }
    
    
    // Just pretty print functions
    public static String toKm(double distance){
        return String.format("%.2f", distance / 1000);
    }
    
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);  
    }
    public static String convertSecondsToMmSs(long ms) {
        long seconds = ms / 1000;
        long h = (seconds / 60 / 60) % 60;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;        
        return String.format("%02dh:%02dm:%02ds", h,m,s);
    }


}
