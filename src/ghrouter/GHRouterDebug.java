package ghrouter;

import com.graphhopper.*;
import com.graphhopper.reader.osm.GraphHopperOSM;
import com.graphhopper.util.CmdArgs;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.InstructionList;
import java.io.IOException;
import java.util.Locale;
import com.graphhopper.util.Parameters;
import static com.graphhopper.util.Parameters.Algorithms.AltRoute.MAX_PATHS;
import static com.graphhopper.util.Parameters.Algorithms.AltRoute.MAX_SHARE;
import static com.graphhopper.util.Parameters.Algorithms.AltRoute.MAX_WEIGHT;

/**
 * This program was used for testing, it expects <latFrom lonFrom latTo lonTo> as arguments and only takes one route
 * However it prints more info about this route, including instructions (turn right etc)
 * See GHRouter for a more flexible program
 * @author bkw5
 */
public class GHRouterDebug {   
    

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
//        if(args.length != 4){
//            badArgs();            
//        }
//        double latFrom = 0, lonFrom = 0, latTo = 0, lonTo = 0;
//        try {
//            latFrom = Double.parseDouble(args[0]);
//            lonFrom = Double.parseDouble(args[1]);
//            latTo = Double.parseDouble(args[2]);
//            lonTo = Double.parseDouble(args[3]);
//        } catch (NumberFormatException numberFormatException) {
//            badArgs();            
//        }
        
        // Examples coords - from memory this is from the UOW to somewhere close to the CBD
        double latFrom = -37.790659;
        double lonFrom = 175.318193;
        double latTo = -37.774955;         
        double lonTo = 175.26927;   
//
//        double latFrom = -38.128615;
//        double lonFrom = 176.220703;
//        double latTo = -37.744318;         
//        double lonTo = 175.240173; 
        //-37.731082%2C175.217514&point=-37.798662%2C175.326347
        
        // Init hopper with settings from config file
        GraphHopper hopper = new GraphHopperOSM().forServer()
                .init(CmdArgs.readFromConfig("config.properties", "graphhopper.config"));    
        
        // Load/download graph -- This takes a while the first time it is done
        hopper.importOrLoad();          
        
        // Get fatest, shortest and energy route for car
        route("fastest", "car", hopper, latFrom, lonFrom, latTo, lonTo);
        route("shortest", "car", hopper, latFrom, lonFrom, latTo, lonTo);
        route("energy", "car", hopper, latFrom, lonFrom, latTo, lonTo);
        
        
//        GHRequest req = new GHRequest(latFrom, lonFrom, latTo, lonTo).
//                setWeighting("fastest").setWeightingCost("energy").
//                setVehicle("car").setAlgorithm("alternative_route").
//                setLocale(Locale.getDefault()); 
//        req.getHints().put(MAX_PATHS, 100);
//        req.getHints().put(MAX_WEIGHT, 10.0);
//        req.getHints().put(MAX_SHARE,.99999);
//        req.getHints().put("alternative_route.min_plateau_factor", 0.0001);
//        req.getHints().put("alternative_route.max_exploration_factor", 10);
//        GHResponse rsp = hopper.route(req);
//        for(PathWrapper p : rsp.getAll()){
//             System.out.println(p.getRouteWeight() + " " +p.getRouteCost());
//         }

    } 
    
    
 
    
    private static void route(String weighting, String vehicle, GraphHopper hopper,
            double latFrom, double lonFrom, double latTo, double lonTo){
        
        GHRequest req = new GHRequest(latFrom, lonFrom, latTo, lonTo).
                setWeighting(weighting).
                setVehicle(vehicle).
                setLocale(Locale.getDefault()).setAlgorithm("dijkstra");
        GHResponse rsp = hopper.route(req); 
        System.out.println("alg " +rsp.getDebugInfo());
        // first check for errors
        if (rsp.hasErrors()) {
            System.out.println("ERROR in calculating route - probably typo?");
            System.out.println(rsp.getErrors());
            return;
        }
        System.out.println("Find best path using " + vehicle.toUpperCase() + " and weighting = " + weighting.toUpperCase());
        // use the best path, see the GHResponse class for more possibilities.
        PathWrapper path = rsp.getBest();        
        
        //PointList pointList = path.getPoints();
        double distance = path.getDistance();
        long timeInMs = path.getTime();  
        
        InstructionList il = path.getInstructions();
        // iterate over every turn instruction
        for (Instruction instruction : il) { 
            System.out.println(padRight(instruction.getTurnDescription(hopper.getTranslationMap().getWithFallBack(Locale.getDefault())),60)
                    + " ( Distance = " + String.format("%.2f", instruction.getDistance()/1000) + "km " 
                    + " time = " + convertSecondsToMmSs(instruction.getTime() / 1000) +")");
            //instruction.getDistance();
            
        }
        System.out.println("\nTotal distance: " + String.format("%.2f", distance / 1000) + "km \nTotal time: " + convertSecondsToMmSs(timeInMs / 1000));
        System.out.println("Total route weight = " + path.getRouteWeight());
        System.out.println("RE: " + path.getRouteCost());
        System.out.println();
    }

    private static void badArgs() {
        System.out.println("Incorrect arguments, please enter the <latFrom lonFrom latTo lonTo> in decimal format");
        System.exit(1);
    }
    
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);  
    }
    
    public static String convertSecondsToMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;        
        return String.format("%02d:%02d", m,s);
    }

   
    
}
