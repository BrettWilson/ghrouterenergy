/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.routing.weighting;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.util.DistanceCalc3D;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PMap;
import com.graphhopper.util.Parameters.Routing;
import com.graphhopper.util.PointList;

/**
 * Calculates the energy consumption on a given edge according to some model from literature * 
 * <p>
 * Right now Jurik et al method is used - you may implement your own
 *
 * @author bkw5
 */
public class EVWeighting extends AbstractWeighting {

    // Barth et al - doesnt seem to be accuarate 
//    double b0 = -0.7123;
//    double b1 = -0.022703;
//    double b2 = -0.0032109;
//    double b3 = 0.00010617;
//    double b4 = -0.00000058308;
//    double b5 = 2.3320;
    
    // Jurik et al
    private final static double a = 113.5;
    private final static double b = 0.774;
    private final static double c = 0.4212;
    private final static double M = 1190;
    private final static double regen = 0.85;
    private final static double u = 0.0088;
    private final static double p = 1.25;
    private final static double A = 2.31;
    private final static double C = 0.32;

    // For calculating gravity constant - NOT NEEDED USE g below
    // Distance of see level from center of Earth (meters)
    //private final static double distCenter = 6380000;
    //private final static double massEarth = 5.98e24;
    //private final static double G = 6.673e-11;
    //private final static double G_EARTH_NUMERATOR = G * massEarth;
    
    // This is gravity acceleration constant - pretty sure this is what is meant to be used
    // Note that it does change depending on elevation - but it is by such a small amount that
    // it is not worth the calculation
    private final static double g = 9.80665;
   

    public EVWeighting(FlagEncoder encoder, PMap pMap) {
        super(encoder);       
    }

    public EVWeighting(FlagEncoder encoder) {
        this(encoder, new PMap(0));
    }

    @Override
    public double getMinWeight(double distance) {
        return 0;
    }

    @Override
    public double calcWeight(EdgeIteratorState edge, boolean reverse, int prevOrNextEdgeId) {
        double speed = reverse ? flagEncoder.getReverseSpeed(edge.getFlags()) : flagEncoder.getSpeed(edge.getFlags());
        if (speed == 0)
            return Double.POSITIVE_INFINITY;
        
        // For the consumption estimation you need "average" speed
        // Here speed is the speed limit, since we do not have access to average speed data
        // I am just assuming that average speed is roughly 80% of speed limit
        // Comment this out to just assume speed is max speed
        speed *= 0.8;        
        
        // Get a list of points (like pins on google maps) with 3d info, i.e. just lat/long + elevation
        PointList pl = edge.fetchWayGeometry(3);
        if (!pl.is3D())
            throw new IllegalStateException("Please enable elevation");

        // Set prev values to first point for now
        double prevEle = pl.getElevation(0);
        double prevLat = pl.getLat(0);
        double prevLon = pl.getLon(0);

        //double totalDistance = edge.getDistance(); //- quick way, not sure if takes into account elevation
        
        // Calculate total distance by summing distance between points
        double totalDistance = 0; 
        DistanceCalc3D distCalc = new DistanceCalc3D();  
        double slope = 0;       
        double count = 0;
        for (int i = 1; i < pl.size(); i++) {
            count += 1.0;

            // Get info about current point
            double lat = pl.getLatitude(i);
            double lon = pl.getLongitude(i);
            double ele = pl.getElevation(i);

            // Difference in elevation from previous point (can be negative if travelled downhill)
            double eleDelta = ele - prevEle;

            // Calculate distance using 3D calculater and exact coords - as accuarate as we gonna get for this API
            double distOfCurrentPoint = distCalc.calcDist(prevLat, prevLon, prevEle, lat, lon, ele);
            
            // Sum distance and set new prev values
            totalDistance += distOfCurrentPoint;
            prevLat = lat;
            prevLon = lon;
            prevEle = ele;

            // We are trying to calculate theta - which is the angle/slope
            // Elevation change is the "rise"/opposite and distance is the hypotenuse
            // note that distance is not the "run"/adjacent since we are taking into account elevation             
            slope += (distOfCurrentPoint > 0 ? eleDelta / distOfCurrentPoint : 0);

        }

        // Get average slope and theta       
        double theta = Math.asin(slope / count); 
//        System.out.println(slope/count);
//        System.out.println(theta);
//        theta = Math.atan((pl.getElevation(pl.size() - 1) - pl.getElevation(0)) / edge.getDistance());
//        System.out.println((pl.getElevation(pl.size() - 1) - pl.getElevation(0)) / edge.getDistance());
//        System.out.println(theta);System.out.println();
//        System.out.println(edge.getDistance());
//        System.out.println(pl.getElevation(pl.size() - 1) - pl.getElevation(0));

        // Calculate Er and Ep according to a model
        double Er = ((p / 2.0) * A * C * (speed * speed) * totalDistance)
                + (M * g * u * totalDistance * Math.cos(theta));
        double Ep = M * g * u * totalDistance * Math.sin(theta);
        //System.out.println(Er + " " + Ep + "\n");
        
        // Return correct value
        if (Ep <= 0)
            return Er + ((regen - 1.0) * Ep);
        else
            return Er;
        
        // Problems I have found with this model (could be model or I could be implementing it wrong)
        // 1.) The speed*speed part really makes the number so much bigger, roads with high speeds
        // e.g. motorways/highways where it will be 100 seem to be punished too much 100*100 >>> 50*50
        // 2.) There is a big difference between cos(theta) and sin(theta) sin is usually much smaller number
        // e.g. 0.01 vs 0.1 it is not clear why cos is used instead of sin and vice versa
        // 3.)  Ep is very small compared to Er, so small it may as well not be there
        
        
        // Old code - leaving here for now
        
               // Init b values
//        b1 = b1 * speed;
//        b2 = b2 * speed * speed;
//        b3 = b3 * speed * speed * speed;
//        b4 = b4 * speed * speed * speed * speed;

        //double Ep = M *;
        // double weight = b0 + (b1 * speed) + (b2 * speed * speed) + (b3 * speed * speed * speed)
        //         + (b4 * speed * speed * speed * speed) + (b5 * slope);
        //double fk = Math.exp(weight);	
//        if(fk > 1000.0){
//        System.out.println("prevEle = " + pl.getElevation(0));
//        System.out.println("currEle = " + pl.getElevation(pl.size() - 1));
//	if(totalElevationDelta < 0)         
//		System.out.println("          eleDelta = " + totalElevationDelta);
//	else System.out.println("eleDelta = " + totalElevationDelta);	
//        //System.out.println("slope = " + slope);
//        System.out.println("speed = " + speed);
//        System.out.println("weight = " + weight);
//        System.out.println("fk = "+ fk);
//System.out.println(b0 + " " + (b1 * speed) + " " +(b2 * speed * speed) +" " + (b3 * speed * speed * speed)
        //         + " " +(b4 * speed * speed * speed * speed) + " " +(b5 * slope));
//        }
//        double time = edge.getDistance() / speed * SPEED_CONV; 
//
//        // add direction penalties at start/stop/via points
//        boolean unfavoredEdge = edge.getBool(EdgeIteratorState.K_UNFAVORED_EDGE, false);
//        if (unfavoredEdge)
//            time += headingPenalty;
//
//        return time;
        //return fk;
    }

//    @Override
//    public long calcMillis(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
//        // TODO move this to AbstractWeighting?
//        long time = 0;
//        boolean unfavoredEdge = edgeState.getBool(EdgeIteratorState.K_UNFAVORED_EDGE, false);
//        if (unfavoredEdge)
//            time += headingPenaltyMillis;
//
//        return time + super.calcMillis(edgeState, reverse, prevOrNextEdgeId);
//    }

    @Override
    public String getName() {
        return "energy";
    }
}
