#!/bin/bash
ACTION=$1
OPTION=$2
OSM_FILE="australia-oceania_new-zealand.pbf"

function printUsage {
 echo
 echo "./GHRouter.sh help            - print this message" 
 echo "./GHRouter.sh build           - build project - make sure to do this at least once and then whenver you change the source code" 
 echo "./GHRouter.sh fullbuild       - build project AND build graphhopper main project - not neccessary unless you changed the core GH code"
 echo "./GHRouter.sh random n        - generate n lines of random input from .osm/.pbf file specified in random-config.properties" 
 echo "./GHRouter.sh file <filename> - read valid input from file" 
 echo
 echo "A valid line of input is : <latFrom lonFrom latTo lonTo> in decimal format"
 echo "You may give the program as many valid lines as you want either through a file or piping (System.in)"
 echo "E.G. -37.788624 175.316555 -37.744318 175.240173 "
 echo "echo \"-37.788624 175.316555 -37.744318 175.240173\" | ./GHRouter.sh - example of piping input"

}

CONFIG=config.properties
if [ ! -f "config.properties" ]; then
  cp config-example.properties $CONFIG
fi

file=$CONFIG

if [ -f "$file" ]
then
  #echo "$file found."

  while IFS='=' read -r key value
  do    
    key=$(echo $key | tr '.' '_')
    if [[ "$key" == "datareader_file" ]]; then
    	OSM_FILE=$value
	break
    fi    
  done < "$file"  
else
  echo "$file not found."
fi

function ensureOsmXml {   
  	 
  if [ ! -s "$OSM_FILE" ]; then
    echo "File not found '$OSM_FILE'. Press ENTER to get it from: $LINK"
    echo "Press CTRL+C if you do not have enough disc space or you don't want to download several MB."
    read -e
      
    echo "## now downloading OSM file from $LINK and extracting to $OSM_FILE"
    
    if [ ${OSM_FILE: -4} == ".pbf" ]; then
       wget -S -nv -O "$OSM_FILE" "$LINK"
    elif [ ${OSM_FILE: -4} == ".ghz" ]; then
       wget -S -nv -O "$OSM_FILE" "$LINK"
       unzip "$FILE" -d "$NAME-gh"
    else    
       # make sure aborting download does not result in loading corrupt osm file
       TMP_OSM=temp.osm
       wget -S -nv -O - "$LINK" | bzip2 -d > $TMP_OSM
       mv $TMP_OSM "$OSM_FILE"
    fi
  
    if [[ ! -s "$OSM_FILE" ]]; then
      echo "ERROR couldn't download or extract OSM file $OSM_FILE ... exiting"
      exit
    fi
  else
    echo "## using existing osm file $OSM_FILE"
  fi
}

NAME="${OSM_FILE%.*}"
LINK=$(echo $NAME | tr '_' '/')
if [[ "$OSM_FILE" == "-" ]]; then
   LINK=
elif [[ ${OSM_FILE: -4} == ".osm" ]]; then 
   LINK="http://download.geofabrik.de/$LINK-latest.osm.bz2"
elif [[ ${OSM_FILE: -4} == ".ghz" ]]; then
   LINK="http://graphhopper.com/public/maps/0.1/$OSM_FILE"      
elif [[ ${OSM_FILE: -4} == ".pbf" ]]; then
   LINK="http://download.geofabrik.de/$LINK-latest.osm.pbf"
else
   # e.g. if directory ends on '-gh'
   LINK="http://download.geofabrik.de/$LINK-latest.osm.pbf"
fi


if [ "$ACTION" = "help" ]; then
 printUsage
 exit

elif [ "$ACTION" = "build" ]; then 
 ant -f . -Dnb.internal.action.name=rebuild clean jar
 exit

elif [ "$ACTION" = "fullbuild" ]; then 
 cd graphhopper
 ./graphhopper.sh buildweb
 cd ..
 ant -f . -Dnb.internal.action.name=rebuild clean jar
 exit

elif [ "$ACTION" = "random" ]; then 
 ensureOsmXml
 java -jar dist/GHRouter.jar "random" "$OPTION"
 exit

elif [ "$ACTION" = "file" ]; then
 ensureOsmXml 
 java -jar dist/GHRouter.jar "file" "$OPTION"
 exit 

else 
 ensureOsmXml
 java -jar dist/GHRouter.jar "$@"
 exit
fi
